import requests
from bs4 import BeautifulSoup


def get_news_links(url_in):
    response = requests.get(url_in)
    soup = BeautifulSoup(response.text, 'html.parser')
    news_links = str(soup.find_all('h2'))
    soup = BeautifulSoup(news_links, 'lxml')
    news_links_list = []
    for a in soup.find_all('a', href=True):
        news_links_list.append(a['href'])

    return news_links_list


def get_news(url_in):
    response = requests.get(url_in)
    soup = BeautifulSoup(response.text, 'html.parser')
    [s.extract() for s in soup('script')]
    title = soup.title.get_text()
    full_text = soup.find_all(class_="content")[0].get_text().split('\n')
    full_text = ' '.join(full_text)

    return title, full_text


def main():
    news_links_list = get_news_links('https://www.pasmi.ru')
    with open('output.htm', 'a', encoding='utf-8') as f:
        for news_link in news_links_list:
            news = get_news(news_link)
            html_str = """
            <h2>{}</h2>
            <p>{}</p>
            """.format(news[0], news[1])
            f.write(html_str)


if __name__ == "__main__":
    main()
